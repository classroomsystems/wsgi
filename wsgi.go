// +build !oracle

// Package wsgi enables embedding Python web applications in Go programs.
package wsgi

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"runtime"
	"strings"
	"unsafe"
)

/*
#cgo !windows pkg-config: python-2.7
#include "Python.h"

int initwsgi(void);
PyObject *new_start_response(long slot);
PyObject *new_request_reader(long slot);
PyObject *new_error_writer(long slot);
PyObject *traceback_to_string(PyObject* tb);
PyTypeObject *pyStringType(void);
*/
import "C"

// MaxThreads limits the number of Python threads created by this package.
// Across all Handlers, at most MaxThreads requests will be handled
// simultaneously. To be effective, this variable must be set before calling
// Initialize().
var MaxThreads = 20

// PythonPath sets the Python library path. If it is nil, the embedded
// interpreter's default path is unchanged. Otherwise, PythonPath completely
// replaces Python's path. To be effective, this variable must be set before
// calling Initialize().
//
// Note: These strings are copied into a static buffer allocated by Python.
// The size of the buffer is outside our control, so Initialize() will simply
// panic if the strings are too long to fit in the buffer. Be conservative.
var PythonPath []string = nil

// IgnoreEnvironment, if true, tells the embedded Python interpreter to ignore
// all environment variables. It is true by default. If you set this to false
// before calling Initialize(), then you can control some of Python's behavior
// by setting variables like PYTHONHOME and PYTHONPATH. This may have security
// implications for your program.
var IgnoreEnvironment = true

// Handler implements the http.Handler interface via WSGI and an embedded
// Python interpreter.
type Handler struct {
	// ErrorLog specifies an optional logger to be used for writes to
	// the wsgi.errors stream. It is also used to write tracebacks for
	// unhandled Python exceptions. If nil, the log package's standard
	// logger will be used, directing error output to os.Stderr.
	ErrorLog    *log.Logger
	app         *C.PyObject
	baseEnviron *C.PyObject
}

type context struct {
	w             http.ResponseWriter
	req           *http.Request
	body          *bufio.Reader
	errorLog      *log.Logger
	pyReader      *C.PyObject
	pyError       *C.PyObject
	status        int
	headerWritten bool
}

// There can be only one Python interpreter, so thread-related state is global.
var (
	contexts        []context // Storage for active request data.
	slots           chan int  // Worker thread pool to limit the number of Python threads.
	wsgiVersion     *C.PyObject
	managerOut      chan error
	managerFinalize chan int
	// PyStringType works around a cgo Windows bug where data
	// exported from a DLL cannot be referenced directly by Go.
	// We get the address of PyString_Type in C at initialization
	// and return it to go via a function.
	// See https://github.com/golang/go/issues/4339
	pyStringType *C.PyTypeObject
)

// Initialize sets up the embedded Python interpreter.
// Note that Python initialization errors will often
// cause the program to exit, rather than returning an
// error here. This is because the embedded Python library
// calls exit(3) or abort(3) on most initialization
// errors.
func Initialize() error {
	managerOut = make(chan error)
	managerFinalize = make(chan int)
	go manager()
	return <-managerOut
}

// Finalize tears down the embedded Python interpreter. Do not call if
// Initialize returned an error.
func Finalize() {
	managerFinalize <- 1
	<-managerOut
}

// Manager manages initialization and finalization of a Python interpreter. It
// runs as a goroutine locked to a single OS thread, since Python needs to be
// initialized and finalized from the same thread.
func manager() {
	runtime.LockOSThread()
	err := initialize()
	if err != nil {
		managerOut <- err
		return
	}
	// If the threading module is ever imported, it must be imported from
	// this thread. Otherwise, Python complains.
	thname := pyString_FromString("threading")
	if thname == nil {
		err := pythonError()
		C.Py_Finalize()
		managerOut <- err
		return
	}
	threading := C.PyImport_Import(thname)
	if threading == nil {
		err := pythonError()
		C.Py_DecRef(thname)
		C.Py_Finalize()
		managerOut <- err
		return
	}
	C.Py_DecRef(thname)
	ts := C.PyEval_SaveThread()
	managerOut <- nil
	<-managerFinalize
	C.PyEval_RestoreThread(ts)
	C.Py_DecRef(threading)
	C.Py_Finalize()
	managerOut <- nil
}

func initialize() error {
	var err error
	if C.Py_IsInitialized() != 0 {
		return fmt.Errorf("wsgi: Python already initialized")
	}
	if IgnoreEnvironment {
		C.Py_IgnoreEnvironmentFlag = 1
	} else {
		C.Py_IgnoreEnvironmentFlag = 0
	}

	if len(PythonPath) != 0 {
		// Set Python's path before initializing Python. This relies on the
		// knowledge that Py_GetPath returns a buffer we can reasonably
		// write to. The documentation says this is a bad idea, but it's
		// exactly what py2exe does, so we're in fairly good company.
		C.Py_FrozenFlag = 1 // Tell Python's Modules/getpath.c to ignore weirdness.
		path := strings.Join(PythonPath, string(os.PathListSeparator))
		pathbuf := cStringBytes(C.Py_GetPath())
		if len(pathbuf) < len(path)+1 {
			panic("Python's path buffer is too small for PythonPath")
		}
		copy(pathbuf, path)
		pathbuf[len(path)] = 0
	}

	C.Py_InitializeEx(0)
	C.PyEval_InitThreads()
	if C.Py_IsInitialized() == 0 {
		return fmt.Errorf("wsgi: Couldn't initialize Python interpreter.")
	}
	if C.initwsgi() == 0 {
		err = pythonError()
		C.Py_Finalize()
		return err
	}
	pyStringType = C.pyStringType()
	if err = makeData(); err != nil {
		C.Py_Finalize()
		return err
	}
	contexts = make([]context, MaxThreads)
	slots = make(chan int, MaxThreads)
	for i := 0; i < MaxThreads; i++ {
		slots <- i
		contexts[i].body = bufio.NewReader(eofReader{})
		contexts[i].pyReader = C.new_request_reader(C.long(i))
		if contexts[i].pyReader == nil {
			err = pythonError()
			C.Py_Finalize()
			return err
		}
		contexts[i].pyError = C.new_error_writer(C.long(i))
		if contexts[i].pyError == nil {
			err = pythonError()
			C.Py_Finalize()
			return err
		}
	}
	return nil
}

// NewHandler loads the named Python module and evaluates appExpr in that
// module's global scope. AppExpr must evaluate to a WSGI application
// callable. Env, if not nil, adds values to the WSGI environment dictionary
// passed to the application with each request.
func NewHandler(module, appExpr string, env map[string]string) (*Handler, error) {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()
	gil := C.PyGILState_Ensure()
	defer C.PyGILState_Release(gil)
	callable := importApp(module, appExpr)
	if callable == nil {
		return nil, pythonError()
	}
	if C.PyCallable_Check(callable) == 0 {
		C.Py_DecRef(callable)
		return nil, fmt.Errorf("wsgi: evaluating %q in %s did not result in a callable object", appExpr, module)
	}
	baseEnviron := makeBaseEnviron(env)
	if baseEnviron == nil {
		C.Py_DecRef(callable)
		return nil, pythonError()
	}
	return &Handler{nil, callable, baseEnviron}, nil
}

// Close releases any memory in Python used for this handler..
func (h *Handler) Close() {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()
	gil := C.PyGILState_Ensure()
	defer C.PyGILState_Release(gil)
	C.Py_DecRef(h.app)
}

// ServeHTTP implements the http.Handler interface.
func (h *Handler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	runtime.LockOSThread()
	defer runtime.UnlockOSThread()
	slot := <-slots
	contexts[slot].w = w
	contexts[slot].req = req
	contexts[slot].body.Reset(req.Body)
	contexts[slot].errorLog = h.ErrorLog
	contexts[slot].status = -1
	contexts[slot].headerWritten = false
	err := h.callWSGI(slot)
	if err != nil {
		if h.ErrorLog != nil {
			h.ErrorLog.Println(err)
		} else {
			log.Println(err)
		}
		if !contexts[slot].headerWritten {
			w.WriteHeader(500)
			w.Write([]byte("Internal error: unhandled Python exception"))
		}
	}
	// Don't leave garbage lying around.
	contexts[slot].w = nil
	contexts[slot].req = nil
	contexts[slot].body.Reset(eofReader{})
	contexts[slot].errorLog = nil
	slots <- slot
}

// CallWSGI calls into Python to handle the request stored in the given slot.
func (h *Handler) callWSGI(slot int) error {
	gil := C.PyGILState_Ensure()
	defer C.PyGILState_Release(gil)
	args := C.PyTuple_New(2)
	if args == nil {
		return pythonError()
	}
	defer C.Py_DecRef(args)
	environ := h.makeEnviron(slot)
	if environ == nil {
		return pythonError()
	}
	// PyTuple_SetItem steals a reference to its argument
	// so we don't have to DecRef it.
	if C.PyTuple_SetItem(args, 0, environ) != 0 {
		return pythonError()
	}
	start_response := C.new_start_response(C.long(slot))
	if start_response == nil {
		return pythonError()
	}
	// PyTuple_SetItem steals a reference to its argument
	// so we don't have to DecRef it.
	if C.PyTuple_SetItem(args, 1, start_response) != 0 {
		return pythonError()
	}
	result := C.PyObject_CallObject(h.app, args)
	if result == nil {
		return pythonError()
	}
	defer C.Py_DecRef(result)
	defer callClose(result)
	iter := C.PyObject_GetIter(result)
	if iter == nil {
		return pythonError()
	}
	defer C.Py_DecRef(iter)
	write := func(slot int, o *C.PyObject) error {
		// TODO: Maybe consolidate write and goWrite.
		if o.ob_type != pyStringType {
			var tname string
			if o.ob_type != nil {
				tname = C.GoString(o.ob_type.tp_name)
			} else {
				tname = "<bad type>"
			}
			return fmt.Errorf("wsgi: can only write byte strings, not %s", tname)
		}
		b := pyString_AsBytes(o)
		ts := C.PyEval_SaveThread()
		defer C.PyEval_RestoreThread(ts)
		if !contexts[slot].headerWritten {
			if contexts[slot].status < 0 {
				return fmt.Errorf("wsgi: start_response not called before writing data")
			}
			if contexts[slot].status != 200 {
				contexts[slot].w.WriteHeader(contexts[slot].status)
			}
			contexts[slot].headerWritten = true
		}
		_, err := contexts[slot].w.Write(b)
		return err
	}

	for {
		o := C.PyIter_Next(iter)
		if o == nil {
			break
		}
		err := write(slot, o)
		C.Py_DecRef(o)
		if err != nil {
			return err
		}
	}
	if C.PyErr_Occurred() != nil {
		return pythonError()
	}
	if !contexts[slot].headerWritten {
		if contexts[slot].status < 0 {
			return fmt.Errorf("wsgi: start_response never called for request")
		}
		contexts[slot].w.WriteHeader(contexts[slot].status)
		contexts[slot].headerWritten = true
	}
	return nil
}

type pyDict struct {
	d            *C.PyObject
	hadException bool
}

func (pd *pyDict) set(key string, value *C.PyObject) {
	if pd.hadException {
		return
	}
	pk := pyString_FromString(key)
	if pk == nil {
		pd.hadException = true
		return
	}
	defer C.Py_DecRef(pk)
	if C.PyDict_SetItem(pd.d, pk, value) != 0 {
		pd.hadException = true
		return
	}
}

func (pd *pyDict) setString(key, value string) {
	if pd.hadException {
		return
	}
	pv := pyString_FromString(value)
	if pv == nil {
		pd.hadException = true
		return
	}
	defer C.Py_DecRef(pv)
	pd.set(key, pv)
}

func makeBaseEnviron(env map[string]string) *C.PyObject {
	environ := C.PyDict_New()
	if environ == nil {
		return nil
	}
	d := &pyDict{environ, false}
	d.setString("SCRIPT_NAME", "")
	d.set("wsgi.version", wsgiVersion)
	d.setString("wsgi.multithread", "true")
	d.setString("wsgi.multiprocess", "")
	d.setString("wsgi.run_once", "")
	for k := range env {
		d.setString(k, env[k])
	}
	if d.hadException {
		C.Py_DecRef(environ)
		return nil
	}
	return environ
}

func (h *Handler) makeEnviron(slot int) *C.PyObject {
	environ := C.PyDict_Copy(h.baseEnviron)
	if environ == nil {
		return nil
	}
	d := &pyDict{environ, false}
	req := contexts[slot].req
	host, _, err := net.SplitHostPort(req.RemoteAddr)
	if err == nil {
		d.setString("REMOTE_ADDR", host)
	}
	d.setString("REQUEST_METHOD", req.Method)
	d.setString("PATH_INFO", req.URL.Path)
	d.setString("QUERY_STRING", req.URL.RawQuery)
	hostPort := strings.SplitN(req.Host, ":", 2)
	d.setString("SERVER_NAME", hostPort[0])
	port := "80"
	if len(hostPort) > 1 && len(hostPort[1]) > 0 {
		port = hostPort[1]
	} else if req.TLS != nil {
		port = "443"
	}
	d.setString("SERVER_PORT", port)
	d.setString("SERVER_PROTOCOL", req.Proto)
	d.setString("CONTENT_TYPE", req.Header.Get("Content-Type"))
	d.setString("CONTENT_LENGTH", req.Header.Get("Content-Length"))
	for k, vs := range req.Header {
		k = "HTTP_" + strings.ToUpper(strings.Replace(k, "-", "_", -1))
		d.setString(k, strings.Join(vs, ","))
	}
	// FIXME: Do TLS variables and anything else appropriate.
	if req.TLS == nil {
		d.setString("wsgi.url_scheme", "http")
	} else {
		d.setString("wsgi.url_scheme", "https")
	}
	d.set("wsgi.input", contexts[slot].pyReader)
	d.set("wsgi.errors", contexts[slot].pyError)
	if d.hadException {
		C.Py_DecRef(environ)
		return nil
	}
	return environ
}

// CallClose calls the close method of o, if there is one, ignoring any error.
func callClose(o *C.PyObject) {
	s := pyString_FromString("close")
	if s == nil {
		C.PyErr_Clear()
		return
	}
	defer C.Py_DecRef(s)
	close := C.PyObject_GetAttr(o, s)
	if close == nil {
		C.PyErr_Clear()
		return
	}
	defer C.Py_DecRef(close)
	r := C.PyObject_CallObject(close, nil)
	if r == nil {
		C.PyErr_Clear()
		return
	}
	C.Py_DecRef(r)
}

//export goWrite
// GoWrite writes the n bytes pointed to by b to the ResponseWriter in slot.
// If the write returns an error, a pointer to the error message string is
// placed in *errOut. This string is allocated with malloc and must be freed by
// the caller. If there is no error, *errOut is set to nil (NULL).
func goWrite(slot C.int, b *C.char, n C.int, errOut **C.char) C.int {
	sl := int(slot)
	if sl < 0 || sl >= len(contexts) {
		// TODO: Bad slot number. We should probably raise a
		// Python exception, but for now, just drop the data on
		// the floor and pretend we succeeded.
		*errOut = nil
		return n
	}
	if !contexts[sl].headerWritten {
		if contexts[sl].status < 0 {
			panic("impossible: the write callable was reached without start_response setting a status code")
		}
		if contexts[slot].status != 200 {
			contexts[sl].w.WriteHeader(contexts[slot].status)
		}
		contexts[slot].headerWritten = true
	}
	nwritten, err := contexts[sl].w.Write(cBytes(unsafe.Pointer(b), int(n)))
	if err != nil {
		*errOut = C.CString(err.Error())
	} else {
		*errOut = nil
	}
	return C.int(nwritten)
}

//export goLogError
// GoLogError prints the n bytes pointed to by b to the slot's ErrorLog,
// or the standard logger.
func goLogError(slot C.int, b *C.char, n C.int) {
	buf := cBytes(unsafe.Pointer(b), int(n))
	sl := int(slot)
	if sl >= 0 && sl < len(contexts) && contexts[sl].errorLog != nil {
		contexts[sl].errorLog.Printf("%s", buf)
	} else {
		log.Printf("%s", buf)
	}
}

//export goAddHeader
// GoAddHeader adds a response header named key
// (length keyLen) with value (length valueLen) to the
// ResponseWriter in slot.
func goAddHeader(slot C.int, key *C.char, keyLen C.int, value *C.char, valueLen C.int) {
	sl := int(slot)
	if sl < 0 || sl >= len(contexts) {
		// TODO: Bad slot number. We should probably raise a
		// Python exception, but for now, just drop the data on
		// the floor and pretend we succeeded.
		return
	}
	k := C.GoStringN(key, keyLen)
	v := C.GoStringN(value, valueLen)
	contexts[sl].w.Header().Add(k, v)
}

//export goSetStatus
// GoSetStatus sets the response status code to be used
// by the ResponseWriter in slot.
func goSetStatus(slot, code C.int) {
	sl := int(slot)
	if sl < 0 || sl >= len(contexts) {
		return
	}
	contexts[sl].status = int(code)
}

//export goHeaderWritten
// GoHeaderWritten returns true if the HTTP header has already been sent for
// the connection in slot.
func goHeaderWritten(slot C.int) C.int {
	sl := int(slot)
	if sl < 0 || sl >= len(contexts) {
		return 0
	}
	if contexts[sl].headerWritten {
		return 1
	}
	return 0
}

//export goReadAll
// GoReadAll reads all remaining bytes from the request body in slot. A
// pointer to the data read is placed in *output, and the number of bytes read
// is returned. If the read returns an error, a pointer to the error message
// string is placed in *errOut. Strings are allocated with malloc and must be
// freed by the caller. If there is no error, *errOut is set to nil (NULL). If
// there is an error, *output will be set to nil (NULL).
func goReadAll(slot C.int, output, errOut **C.char) C.int {
	sl := int(slot)
	*output = nil
	*errOut = nil
	if sl < 0 || sl >= len(contexts) {
		// TODO: Bad slot number. We should probably raise a
		// Python exception, but for now, just pretend we hit EOF.
		*output = C.CString("")
		return 0
	}
	buf, err := ioutil.ReadAll(contexts[sl].body)
	if err != nil {
		*errOut = C.CString(err.Error())
		return 0
	}
	*output = C.CString(string(buf))
	return C.int(len(buf))
}

//export goReadN
// GoReadN reads from the request body in slot, attempting to fill the n-byte
// buffer pointed to by buf. It returns the number of bytes placed in buf.  If
// the read returns an error, a pointer to the error message string is placed
// in *errOut. This string is allocated with malloc and must be freed by the
// caller. If there is no error, *errOut is set to nil (NULL).
func goReadN(slot C.int, buf *C.char, n C.int, errOut **C.char) C.int {
	sl := int(slot)
	*errOut = nil
	if sl < 0 || sl >= len(contexts) {
		// TODO: Bad slot number. We should probably raise a
		// Python exception, but for now, just pretend we hit EOF.
		return 0
	}
	b := cBytes(unsafe.Pointer(buf), int(n))
	nr, err := io.ReadFull(contexts[sl].body, b)
	if err != nil && err != io.EOF && err != io.ErrUnexpectedEOF {
		*errOut = C.CString(err.Error())
	}
	return C.int(nr)
}

//export goReadLineAll
// GoReadLineAll reads a full line from the request body in slot. A pointer
// to the data read is placed in *output, and the number of bytes read is
// returned. If the read returns an error, a pointer to the error message
// string is placed in *errOut. Strings are allocated with malloc and must be
// freed by the caller. If there is no error, *errOut is set to nil (NULL). If
// there is an error, *output will be set to nil (NULL).
func goReadLineAll(slot C.int, output, errOut **C.char) C.int {
	sl := int(slot)
	*output = nil
	*errOut = nil
	if sl < 0 || sl >= len(contexts) {
		// TODO: Bad slot number. We should probably raise a
		// Python exception, but for now, just pretend we hit EOF.
		*output = C.CString("")
		return 0
	}
	line, err := contexts[sl].body.ReadString('\n')
	if err != nil && err != io.EOF {
		*errOut = C.CString(err.Error())
		return 0
	}
	*output = C.CString(line)
	return C.int(len(line))
}

//export goReadLineN
// GoReadLineN reads a line no longer than n bytes from the request body in
// slot into the buffer pointed to by buf. It returns the number of bytes
// placed in buf.  If the read returns an error, a pointer to the error
// message string is placed in *errOut. This string is allocated with malloc
// and must be freed by the caller. If there is no error, *errOut is set to
// nil (NULL).
func goReadLineN(slot C.int, buf *C.char, n C.int, errOut **C.char) C.int {
	sl := int(slot)
	*errOut = nil
	if sl < 0 || sl >= len(contexts) {
		// TODO: Bad slot number. We should probably raise a
		// Python exception, but for now, just pretend we hit EOF.
		return 0
	}
	b := cBytes(unsafe.Pointer(buf), int(n))
	var err error
	var i int
	for i = 0; i < len(b); i++ {
		b[i], err = contexts[sl].body.ReadByte()
		if err == io.EOF {
			break
		}
		if err != nil {
			*errOut = C.CString(err.Error())
			break
		}
		if b[i] == '\n' {
			i++
			break
		}
	}
	return C.int(i)
}

func makeData() error {
	wsgiVersion = C.PyTuple_New(2)
	if wsgiVersion == nil {
		return pythonError()
	}
	one := C.PyInt_FromLong(C.long(1))
	if one == nil {
		return pythonError()
	}
	// PyTuple_SetItem steals a reference to its argument
	// so we don't have to DecRef it.
	if C.PyTuple_SetItem(wsgiVersion, 0, one) != 0 {
		return pythonError()
	}
	zero := C.PyInt_FromLong(C.long(0))
	if zero == nil {
		return pythonError()
	}
	// PyTuple_SetItem steals a reference to its argument
	// so we don't have to DecRef it.
	if C.PyTuple_SetItem(wsgiVersion, 1, zero) != 0 {
		return pythonError()
	}
	return nil
}

func importApp(module, appExpr string) *C.PyObject {
	mname := pyString_FromString(module)
	if mname == nil {
		return nil
	}
	defer C.Py_DecRef(mname)
	mod := C.PyImport_Import(mname)
	if mod == nil {
		return nil
	}
	defer C.Py_DecRef(mod)
	cExpr := C.CString(appExpr)
	defer C.free(unsafe.Pointer(cExpr))
	return C.PyRun_StringFlags(cExpr, C.Py_eval_input, C.PyModule_GetDict(mod), nil, nil)
}

// PythonError is the type used to represent exceptions that happen in Python
// code.
type PythonError struct {
	Exception string
	Traceback string
}

func (e PythonError) Error() string {
	// Python itself prints the exception after the
	// traceback, so we follow suit.
	return "Python exception: " + e.Traceback + e.Exception
}

// PythonError returns the current Python exception as an error
// value, clearing the exception state in the process.
func pythonError() error {
	var exc, val, tb *C.PyObject
	var err PythonError
	C.PyErr_Fetch(&exc, &val, &tb)
	C.PyErr_NormalizeException(&exc, &val, &tb)
	if exc == nil {
		return nil
	}
	defer C.Py_DecRef(exc)
	defer C.Py_DecRef(val)
	if tb != nil {
		defer C.Py_DecRef(tb)
	}
	excs := C.PyObject_Str(val)
	if excs == nil {
		C.PyErr_Clear()
		return PythonError{"Double exception, bailing!", "No traceback, sorry.\n"}
	}
	defer C.Py_DecRef(excs)
	err.Exception = pyString_AsString(excs)
	if tb == nil {
		err.Traceback = "No traceback\n"
		return err
	}
	tbs := C.traceback_to_string(tb)
	if tbs == nil {
		C.PyErr_Clear()
		return PythonError{"Double exception, bailing!", "No traceback, sorry.\n"}
	}
	defer C.Py_DecRef(tbs)
	err.Traceback = pyString_AsString(tbs)
	return err
}

func pyString_FromString(s string) *C.PyObject {
	// TODO: Maybe cache some commonly used strings so we don't have to
	// reallocate them every time.
	cs := C.CString(s)
	defer C.free(unsafe.Pointer(cs))
	return C.PyString_FromStringAndSize(cs, C.Py_ssize_t(len(s)))
}

func pyString_AsString(s *C.PyObject) string {
	var cs *C.char
	var clen C.Py_ssize_t
	n := C.PyString_AsStringAndSize(s, &cs, &clen)
	if n < 0 {
		// The docs say this shouldn't happen unless &clen is nil.
		// If it does, though, a TypeError exception has been raised in Python.
		panic(fmt.Sprintf("!!TYPE ERROR: %v!!", pythonError()))
	}
	return C.GoStringN(cs, C.int(clen))
}

func pyString_AsBytes(s *C.PyObject) []byte {
	var cs *C.char
	var clen C.Py_ssize_t
	n := C.PyString_AsStringAndSize(s, &cs, &clen)
	if n < 0 {
		// The docs say this shouldn't happen unless &clen is nil.
		// If it does, though, a TypeError exception has been raised in Python.
		panic(fmt.Sprintf("!!TYPE ERROR: %v!!", pythonError()))
	}
	// This makes a copy of the data:
	// return C.GoBytes(unsafe.Pointer(cs), C.int(clen))
	// This doesn't, but make sure the []byte doesn't
	// live longer than the Python string:
	return cBytes(unsafe.Pointer(cs), int(clen))
}

// CBytes returns a []byte which accesses the n bytes pointed to by p.
// N must not be greater than 1<<30.
func cBytes(p unsafe.Pointer, n int) []byte {
	return (*[1 << 30]byte)(p)[:n:n]
}

// CStringBytes returns a []byte which accesses the zero-terminated C string s.
// The string's length must be less than 1<<30.
func cStringBytes(s *C.char) []byte {
	var i int
	b := (*[1 << 30]byte)(unsafe.Pointer(s))
	for i = 0; i < len(b); i++ {
		if b[i] == 0 {
			return b[:i+1 : i+1]
		}
	}
	panic("unterminated C string")
}

type eofReader struct{}

func (eofReader) Read([]byte) (int, error) {
	return 0, io.EOF
}

func (eofReader) Close() error {
	return nil
}
