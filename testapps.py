import sys

# Import threading to be sure it doesn't cause an error message.
# Older versions of this package did so.
import threading

def hello_func(environ, start_response):
	start_response("200 OK", [
		("Content-Type", "text/plain"),
	])
	return [b"Hello, World!\n"]
	
class HelloClass(object):
	def __init__(self, environ, start_response):
		self.eviron = environ
		self.start_response = start_response
	def __iter__(self):
		self.start_response("200 OK", [
			("Content-Type", "text/plain"),
		])
		yield b"Hello, World!\n"
		
class HelloOldClass:
	def __init__(self, environ, start_response):
		self.eviron = environ
		self.start_response = start_response
	def __iter__(self):
		self.start_response("200 OK", [
			("Content-Type", "text/plain"),
		])
		yield b"Hello, World!\n"
		
class HelloObj:
	def __call__(self, environ, start_response):
		start_response("200 OK", [
			("Content-Type", "text/plain"),
		])
		return [b"Hello, World!\n"]

def bad_args(foo=None, environ=None, start_response=None):
	start_response("200 OK", [
		("Content-Type", "text/plain"),
	])
	return [b"Hello, World!\n"]

def succeed(environ, start_response, output=b"OK"):
	"""Succeed is a generic successful responder used in tests below."""
	start_response("200 OK", [
		("Content-Type", "text/plain"),
	])
	return [output]

def fail(environ, start_response, output=b"FAIL"):
	"""Fail is a generic failure responder used in tests below."""
	start_response("500 Internal Server Error", [
		("Content-Type", "text/plain"),
	])
	return [output]

def remote_addr(environ, start_response):
	if "REMOTE_ADDR" not in environ:
		return succeed(environ, start_response, b"None")
	return succeed(environ, start_response, bytes(environ["REMOTE_ADDR"]))

def misbehave(environ, start_response):
	if environ["PATH_INFO"] == "/bad_call":
		start_response(response_headers=[
				("Content-Type", "text/plain;charset=utf-8"),
			],
			status="200 OK",
		)
		return [b"Should have failed."]
	elif environ["PATH_INFO"] == "/bad_status":
		start_response("Not OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		return [b"Should have failed."]
	elif environ["PATH_INFO"] == "/bad_headers":
		start_response("200 OK", [
			["Content-Type", "text/plain;charset=utf-8"],
		])
		return [b"Should have failed."]
	elif environ["PATH_INFO"] == "/bad_iter":
		start_response("200 OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		return 42
	elif environ["PATH_INFO"] == "/bad_string":
		start_response("200 OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		return [u"Should have failed."]
	elif environ["PATH_INFO"] == "/no_start_response":
		return [b"Should have failed."]
	return succeed(environ, start_response, b"Should have failed")

def echo_read(environ, start_response):
	start_response("200 OK", [
		("Content-Type", "text/plain;charset=utf-8"),
	])
	ret = []
	if environ["PATH_INFO"] == "/read":
		ret.append(environ["wsgi.input"].read())
	elif environ["PATH_INFO"] == "/readN":
		while 1:
			s = environ["wsgi.input"].read(8192)
			if s == "":
				break
			ret.append(s)
	elif environ["PATH_INFO"] == "/readline":
		while 1:
			s = environ["wsgi.input"].readline()
			if s == "":
				break
			ret.append(s)
	elif environ["PATH_INFO"] == "/readlineN":
		while 1:
			s = environ["wsgi.input"].readline(8192)
			if s == "":
				break
			ret.append(s)
	elif environ["PATH_INFO"] == "/readlines":
		ret = environ["wsgi.input"].readlines()
	elif environ["PATH_INFO"] == "/iter":
		for line in environ["wsgi.input"]:
			ret.append(line)
	return ret

def echo_write(environ, start_response):
	ret = []
	if environ["PATH_INFO"] not in ("/list", "/str", "/callback"):
		return fail(environ, start_response, "bad path")
	write = start_response("200 OK", [
		("Content-Type", "text/plain;charset=utf-8"),
	])
	while 1:
		s = environ["wsgi.input"].read(8192)
		if s == "":
			break
		if environ["PATH_INFO"] == "/callback":
			write(s)
		else:
			ret.append(s)
	if environ["PATH_INFO"] == "/str":
		return ["".join(ret)]
	return ret

def echo_yield(environ, start_response):
	start_response("200 OK", [
		("Content-Type", "text/plain;charset=utf-8"),
	])
	while 1:
		s = environ["wsgi.input"].read(8192)
		if s == "":
			break
		yield s

def echo_iter_write_callback(environ, start_response):
	write = start_response("200 OK", [
		("Content-Type", "text/plain;charset=utf-8"),
	])
	for line in environ["wsgi.input"]:
		write(line)
	return []

def write_errors(environ, start_response):
	environ["wsgi.errors"].write(b"plain write\n")
	environ["wsgi.errors"].flush()
	environ["wsgi.errors"].writelines([
		b"writelines 1\n",
		b"writelines 2\n",
		b"writelines 3\n",
	])
	return succeed(environ, start_response)

def raiser(environ, start_response):
	if environ["PATH_INFO"] == "/right_away":
		assert False
	elif environ["PATH_INFO"] == "/after_start_response":
		start_response("200 OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		assert False
	elif environ["PATH_INFO"] == "/pass_to_start_response":
		try:
			assert False
		except AssertionError as e:
			start_response("503 Service unavailable", [
				("Content-Type", "text/plain;charset=utf-8"),
			], sys.exc_info())
	elif environ["PATH_INFO"] == "/pass_to_start_response2":
		start_response("200 OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		try:
			assert False
		except AssertionError as e:
			start_response("503 Service unavailable", [
				("Content-Type", "text/plain;charset=utf-8"),
			], sys.exc_info())
	elif environ["PATH_INFO"] == "/pass_to_start_response3":
		start_response("200 OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		yield b"NOT OK"
		try:
			assert False
		except AssertionError as e:
			start_response("503 Service unavailable", [
				("Content-Type", "text/plain;charset=utf-8"),
			], sys.exc_info())
	elif environ["PATH_INFO"] == "/after_data":
		start_response("200 OK", [
			("Content-Type", "text/plain;charset=utf-8"),
		])
		yield b"NOT OK"
		assert False
	yield b"OK"

def no_content(environ, start_response):
	start_response("418 I'm a teapot", [
		("Content-Type", "text/plain;charset=utf-8"),
	])
	return []
	
def set_headers(environ, start_response):
	write = start_response("200 OK", [
		("Content-Type", "text/plain;charset=utf-8"),
		("X-Header-Foo", "Foo is the value"),
		("X-Header-Bar", "Bar is the value"),
		("X-Header-Bar", "Bar has another value"),
	])
	return []
